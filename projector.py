import serial
import time

#
# Best I can tell these are brand specific, but not model specific. So for now
# I am just listing brands and their commands.
#
PROJECTORS = {
	# From https://www.optoma.de/uploads/RS232/DS309-RS232-en.pdf
	'OPTIMA': {
		'power on': b'~0000 1\r',
		'power off': b'~0000 2\r',
	},
	# From Sharp XV-Z2000 manual http://files.sharpusa.com/Downloads/ForHome/HomeEntertainment/FrontProjectors/Manuals/hom_man_XVZ2000.pdf
	'SHARP': {
		'power on': b'POWR   1\r',
		'power off': b'POWR   2\r',
	}
}


class Projector:

	def __init__(self, brand, device_id):
		self._device = PROJECTORS[brand]
		self._device_id = device_id

	def send_command(self, command):
		ser = serial.Serial(
				'/dev/%s'%(self._device_id),
				baudrate=9600,
				timeout=0,
				xonxoff=False,
				rtscts=False,
				dsrdtr=False,
				parity=serial.PARITY_NONE,
				bytesize=serial.EIGHTBITS,
				stopbits=serial.STOPBITS_ONE)
		ser.flushInput()
		ser.flushOutput()
		print('writing ', command, ' on ', ser.name)
		ser.write(self._device[command])
		time.sleep(0.5)
		print(ser.read_until())
		ser.close()
