# AtmosFX Control

Use a Raspberry Pi to turn a projector on at the specified time and play a playlist of video fullscreen on VLC.

## Hardware Requirements

- [Raspberry Pi](https://www.raspberrypi.com/products/raspberry-pi-1-model-a-plus/)
- [SD Card](https://amzn.to/3vBDGRA)
- [USB to Serial Cable](https://amzn.to/3vyjFeU) (to control the projector)
- [HDMI cable](https://amzn.to/3B6Tpcz)
- Supported Projector (see [Supported Projectors](#supported-projectors))
- Sound System (mine is left on, a simple smart plug could be used to turn it on/off)

## Installation

1. Clone this repo to the home directory on your Raspberry Pi (the scripts assume `/home/pi`)
2. Run `crontab -e` and add the following to the bottom on your crontab file:

```crontab
30 18 \* \* /home/pi/atmosfx-control/start.py
0 21 _ \* \* /home/pi/atmosfx-control/stop.py
```

This will start the show at 6:30PM local time and end it at 9:00PM. You can adjust the times as needed, [Crontab Guru](https://crontab.guru) is a good cheatsheet for scheduling in cron.

## Supported Projectors

To change which brand specific protocol is used to control the projector, edit the `BRAND_PROTOCOL` constant at the top of the start/stop scripts. Currently only `SHARP` and `OPTIMA` are supported. PRs are welcome if you have another brand you can verify.


```txt
pi@halloween:~/atmosfx-control $ ./start.py
writing  power on  on  /dev/ttyUSB0
b'F'
VLC media player 3.0.11 Vetinari (revision 3.0.11-0-gdc0c5ced72)
[00f606e8] main interface error: no suitable interface module
[00ed8b58] main libvlc error: interface "globalhotkeys,none" initialization failed
[00ed8b58] main libvlc: Running vlc with the default interface. Use 'cvlc' to use vlc without interface.
[00f606e8] skins2 interface error: cannot initialize OSFactory
[00f606e8] [cli] lua interface: Listening on host "*console".
VLC media player 3.0.11 Vetinari
Command Line Interface initialized. Type `help' for help.
> [6270eec0] mmal_codec decoder: VCSM init succeeded: Legacy
[6270eec0] main decoder error: buffer deadlock prevented
[73505008] gles2 generic error: parent window not available
[73501f18] mmal_xsplitter vout display error: Failed to open Xsplitter:opengles2 module
[73505008] xcb generic error: window not available
[73501f18] mmal_xsplitter vout display error: Failed to open Xsplitter:xcb_x11 module


[6270bfe8] mmal_codec decoder: VCSM init succeeded: Legacy
[6270bfe8] main decoder error: buffer deadlock prevented
[60022a80] gles2 generic error: parent window not available
[600322c0] mmal_xsplitter vout display error: Failed to open Xsplitter:opengles2 module
[60022a80] xcb generic error: window not available
[600322c0] mmal_xsplitter vout display error: Failed to open Xsplitter:xcb_x11 module
[62730348] mmal_codec decoder: VCSM init succeeded: Legacy
[62730348] main decoder error: buffer deadlock prevented
[6252e118] gles2 generic error: parent window not available
[6253a640] mmal_xsplitter vout display error: Failed to open Xsplitter:opengles2 module
[6252e118] xcb generic error: window not available
[6253a640] mmal_xsplitter vout display error: Failed to open Xsplitter:xcb_x11 module
```
