#! /usr/bin/python3
import os
from projector import *

VIDEO_FILE = "file:///home/pi/Videos/AtmosFX/Pumpkins.xspf"

projector = Projector('OPTIMA', 'ttyUSB0')

projector.send_command('power on')
os.system("vlc --loop --fullscreen " + VIDEO_FILE)
exit()
